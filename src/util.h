#pragma once
#include<iostream>
#include<cstdio>
#include<cstring>
#include"koopa.h"

using namespace std;

static int debug = 0;//调试模式

// 所有 AST 的基类
class KIR{
    public:
    string s = "";
    bool end = 0;
    string type = "i32";

    KIR(){}
    KIR(string _s){this->s = _s;}
};

class R_code{
public:
  string code = "";
  R_code(){}
  R_code(string _s){
    this->code = _s;
  }
};

class R_Info{
  public:
    string func_name = "";
    int stack_bio = 0;
    int stack_szie = 0;
    //string reg = "t0";
    koopa_raw_slice_t func_param;
    koopa_raw_slice_t bb_param;
    R_Info(){};
};

class BaseAST {
 public:
  virtual ~BaseAST() = default;
  virtual KIR Dump() const = 0;
};