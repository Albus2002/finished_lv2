#pragma once
#include<iostream>
#include<cstring>
#include<memory>
#include "util.h"

using namespace std;

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST {
 public:
  // 用智能指针管理对象
  std::unique_ptr<BaseAST> func_def;
  KIR Dump() const override {
    string s = "fun ";
    string tmp = func_def->Dump().s;
    return KIR(s+tmp);
  }
};

// FuncDef 也是 BaseAST

class FuncDefAST : public BaseAST {
 public:
  std::unique_ptr<BaseAST> func_type;
  std::string ident;
  std::unique_ptr<BaseAST> block;
  KIR Dump() const override {
    if(ident == "main"){
        string s = "@"+ident+"(): " 
                   + func_type->Dump().s 
                   +"{\n"
                   +block->Dump().s
                   +"\n}";
        return KIR(s);
    }
  }
    
};

class FuncTypeAST : public BaseAST{
public:
 std::string func_type;
 KIR Dump() const override{
    string s = "";
    if(func_type=="int"){
        s += "i32 ";
    }
    return KIR(s);
 }

};

class BlockAST:public BaseAST{
public:
    std::unique_ptr<BaseAST> stmt;
    BlockAST() = default;
    KIR Dump() const override{
        string s = "%entry:\n"
                   +stmt->Dump().s;
        return KIR(s);
    }
};

class StmtAST : public BaseAST{
public:
    std::unique_ptr<BaseAST> number;
    StmtAST() = default;
    KIR Dump() const override{
        string s = " ret " 
                + number->Dump().s;
        return KIR(s);
    }
};

class NumberAST : public BaseAST {
public:
    // -- INT_CONST
    int int_const = 0;
    NumberAST() = default;
    NumberAST(int _int_const) : int_const(_int_const) {}

    KIR Dump() const override {
        string s = to_string(int_const);
        return KIR(s);
    }
};

// ...

